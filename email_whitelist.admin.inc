<?php

/**
 * @file
 * Administrative settings for email_whitelist
 */

/**
 * Form builder function for module settings.
 */
function email_whitelist_admin_settings() {
  $form['#prefix'] = t("Allow only these listed email address and domains.");
  $form['email_whitelist_emails_addresses'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#default_value' => variable_get('email_whitelist_emails_addresses', ""),
    '#description' => t('Enter comma seperated emails (with/without quotes). <br />Example:<i>amritbera@gmail.com,abcd@example.com,ijkl@xyz.com</i>'),
  );
  $form['email_whitelist_domains'] = array(
    '#type' => 'textarea',
    '#title' => t('Domains'),
    '#default_value' => variable_get('email_whitelist_domains', ""),
    '#description' => t('Enter comma seperated domains (with/without quotes). <br />Example:<i>yahoo.com,gmail.com,example.com,xyz.com</i>'),
  );
  $form["email_whitelist_negate"] = array(
    "#type" => "checkbox",
    "#title" => t("Negate - Deny these emails and domains."),
    '#default_value' => variable_get('email_whitelist_negate', 0),
  );

  $form['#vaidation'][] = "email_whitelist_admin_settings_validate";

  return system_settings_form($form);
}

function email_whitelist_admin_settings_validate($form, &$form_state) {

  $emails = $form_state['values']['email_whitelist_emails_addresses'];
  if ($emails != "") {
    $text = email_whitelist_admin_settings_cleanup($emails);
    $form_state['values']['email_whitelist_emails_addresses'] = $text;
    $email_array = explode(",", $text);
    foreach ($email_array as $email) {
      $valid = valid_email_address($email);
      if (!$valid) {
        form_set_error("email_whitelist_emails_addresses", "Invalid email address: <i>$email</i>");
      }
    }
  }

  $hosts = $form_state['values']['email_whitelist_domains'];
  if ($hosts != "") {
    $text = email_whitelist_admin_settings_cleanup($hosts);
    $form_state['values']['email_whitelist_domains'] = $text;
    $host_array = explode(",", $text);
    foreach ($host_array as $host) {
      $valid = email_whitelist_admin_settings_domain_name($host);
      if (!$valid) {
        form_set_error("email_whitelist_domains", "Invalid host name: <i>$host</i>");
      }
    }
  }
}

function email_whitelist_admin_settings_cleanup($arg) {
  $text = str_replace(array(' ', '(', ')', '&quot;'), "", check_plain($arg));
  // Check if the last char is a comma.
  $last_char = substr($text, -1);
  if ($last_char == ",") {
    return substr($text, 0, -1);
  } 
  else {
    return $text;
  }
}

function email_whitelist_admin_settings_domain_name($domain_name) {
  $pieces = explode(".", $domain_name);
  if (count($pieces) < 2)
    return FALSE;
  foreach ($pieces as $piece) {
    if (!preg_match('/^[a-z\d][a-z\d-]{0,62}$/i', $piece) || preg_match('/-$/', $piece)) {
      return FALSE;
    }
  }
  return TRUE;
}
