This module helps you to specify a list of email address (comma separated
values). After you save the settings, During user-registration process, this
module will compare the email address entered with the list saved by admin.
If it matches, only then the new user can successfully register.
Example: efgh@gmail.com,"abcd@example.com",ijkl@xyz.com (so, only these email
addresses are allowed to register in the site)

Apart from email address, you can also specify host names (comma separated
values) in the similar way. 
Example: yahoo.com,gmail.com,example.com,adf.com,asdf.in (so, only email
addresses with these host names are allowed to register)

There is also a "negate" option - which does the opposite. If this option is
enabled, it allows any email/host other than the listed ones.

There is "Access Rules" for the similar kind of functionality, but this is much
more user friendly and works directly with csv files.
